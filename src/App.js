import { BrowserRouter, Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Listing from "./pages/Listing";
import Item from "./pages/Item";

const App = () => {
	return (
		<BrowserRouter>
			<Header />
			<Routes>
				<Route
					path="/"
					element={<Listing/>}
				/>
				<Route
					path="/item/:id"
					element={<Item/>}
				/>
			</Routes>
		</BrowserRouter>
	);
}

export default App;

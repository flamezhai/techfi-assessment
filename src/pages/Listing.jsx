import React, { useEffect, useState } from "react";
import axios from "axios";
import LoadingComponent from "../components/LoadingComponents";
import { useNavigate  } from 'react-router-dom';

const Listing = () => {
	const [isLoading, setLoading] = useState(true);
	const [listingData, setListingData] = useState([]);
    const [listingPagingIds, setPagingIds] = useState([]);
	const [currentPage, setCurrentPage] = useState(1);
	const [totalPage, setTotalPage] = useState(0);
    const chunkSize = 50;
    const navigate = useNavigate ();

    useEffect(() =>  {
        // initialize page and get all listing data (500 listings)
        try {
            axios.get('https://hacker-news.firebaseio.com/v0/topstories.json')
                .then(response => {
                    // split them into 50 items per page
                    let chunks = [];
                    if(response['data']) {
                        for (let i = 0; i < response['data'].length; i += chunkSize) {
                            chunks.push(response['data'].slice(i, i + chunkSize));
                        }
                    }
                    setPagingIds(chunks);
                    setTotalPage(chunks.length);

                    // load the first 50 items
                    reloadListing(chunks, currentPage);
                })
                .catch(error => {
                    alert(error);
                });
        } catch (cError) {
            alert(cError);
        }
    }, []);

    const reloadListing = (paramChunks, paramPage) => {
        // Retrieve listing details
        let currentChunk = paramChunks[paramPage];
        try {
            let listingItems = [];
            currentChunk.forEach((itemId, index) => {
                axios.get(`https://hacker-news.firebaseio.com/v0/item/${itemId}.json`)
                    .then(response => {
                        if(response['data']) {
                            let now = new Date();
                            let differentInMinutes = Math.floor((now - new Date(response['data'].time * 1000)) / (1000 * 60));
                            let differentInHours = Math.floor(differentInMinutes / 60);
                            if(differentInMinutes > 60) {
                                response['data'].time = differentInHours > 1 ? `${differentInHours} hours` : `${differentInHours} hour`;
                            } else {
                                response['data'].time = differentInMinutes > 1 ? `${differentInMinutes} minutes` : `${differentInMinutes} minute`;
                            }

                            response['data'].number = ((paramPage - 1) * chunkSize) + index + 1;

                            listingItems[index] = response['data'];
                            if(index === (chunkSize - 1)) {
                                setListingData(listingItems);
                                setLoading(false);
                            }
                        }
                    })
                    .catch(error => {
                        console.log(error);
                        setLoading(false);
                    });
            });
        } catch (cError) {
            alert(cError);
            setLoading(false);
        }
	}
    
	const previous = () => {
        let page = currentPage - 1;
        setCurrentPage(page);
        setLoading(true);
        reloadListing(listingPagingIds, page);
	};
    
	const next = () => {
        let page = currentPage + 1;
        setCurrentPage(page);
        setLoading(true);
        reloadListing(listingPagingIds, page);
	};
    
	const redirectToItem = (itemId) => {
        navigate(`/item/${itemId}`);
	};

    return (
        <section className="content-body">
            <div className="listing-wrapper">
                {/* to show loading component */}
                {
                    (function() {
                        if (isLoading) {
                            
                            return (
                                <div className="position-fixed">
                                    <LoadingComponent></LoadingComponent>
                                </div>
                            );
                        } else {
                        }
                    }) ()
                }
                <div className="container">
                    <table id="new-hacker" className="table">
                        <thead>
                            <tr>
                                <td>No.</td>
                                <td>Title</td>
                                <td>URL</td>
                                <td>Point</td>
                                <td>By</td>
                                <td>Post Time</td>
                                <td>Comments</td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                listingData.map(list=>(
                                    <tr onClick={() => redirectToItem(list.id)} key={list.id}>
                                        <td>{list.number}</td>
                                        <td>{list.title}</td>
                                        <td><a href={list.url}>{list.url ? new URL(list.url).hostname.replace(/^www\./, '') : ""}</a></td>
                                        <td>{list.score}</td>
                                        <td>{list.by}</td>
                                        <td>{list.time}</td>
                                        <td>{list.descendants}</td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    {
                        (function() {
                            if(totalPage > 1) {
                                return (
                                    <div className="paging">
                                        {
                                            currentPage > 1 && 
                                            <span onClick={previous}>Previous</span>
                                        }
                                        {
                                            currentPage < totalPage && 
                                            <span onClick={next}>Next</span>
                                        }
                                    </div>
                                );
                            }
                        }) ()
                    }
                </div>
            </div>
        </section>
    );
}

export default Listing
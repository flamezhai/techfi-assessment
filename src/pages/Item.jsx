import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from 'react-router-dom';

const Item = () => {
    const gParams = useParams();
	const [itemData, setItemData] = useState([]);
	const [comments, setComments] = useState([]);
	// var replies = [];
    const restrictMaxReplyLayer = 2;
	const [hiddenDivs, setHiddenDiv] = useState([]);

    useEffect(() =>  {
        // initialize page and get all listing data (500 listings)
        try {
            axios.get(`https://hacker-news.firebaseio.com/v0/item/${gParams.id}.json`)
                .then(response => {
                    if(response['data']) {
                        let now = new Date();
                        let differentInMinutes = Math.floor((now - new Date(response['data'].time * 1000)) / (1000 * 60));
                        let differentInHours = Math.floor(differentInMinutes / 60);
                        if(differentInMinutes > 60) {
                            response['data'].time = differentInHours > 1 ? `${differentInHours} hours` : `${differentInHours} hour`;
                        } else {
                            response['data'].time = differentInMinutes > 1 ? `${differentInMinutes} minutes` : `${differentInMinutes} minute`;
                        }
                        response['data'].displayUrl = response['data'].url ? new URL(response['data'].url).hostname.replace(/^www\./, '') : "";

                        setItemData(response['data']);
                        if(response['data'].kids) {
                            getComments(response['data'].kids, 0).then((comments) => {
                                // console.log(comments);
                                // console.log(replies);
                                setComments(comments);
                            });;
                        }
                    }
                })
                .catch(error => {
                    alert(error);
                });
        } catch (cError) {
            alert(cError);
        }
    }, []);

    const getComments = async (commentsKids, layer) => {
        let promise = commentsKids.map( async (kidId) => {
            let currentComment = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${kidId}.json`)
                .then(async commentData => {
                    if(commentData['data'] && !commentData['data'].deleted) {
                        let now = new Date();
                        let differentInMinutes = Math.floor((now - new Date(commentData['data'].time * 1000)) / (1000 * 60));
                        let differentInHours = Math.floor(differentInMinutes / 60);
                        if(differentInMinutes > 60) {
                            commentData['data'].time = differentInHours > 1 ? `${differentInHours} hours` : `${differentInHours} hour`;
                        } else {
                            commentData['data'].time = differentInMinutes > 1 ? `${differentInMinutes} minutes` : `${differentInMinutes} minute`;
                        }
                        commentData['data'].kidsComment = [];
                        if(commentData['data'].kids && (commentData['data'].kids).length > 0 && layer < restrictMaxReplyLayer) {
                            commentData['data'].kidsComment = await getComments(commentData['data'].kids, (layer + 1));
                        }
                        // if(commentData['data'].kids && (commentData['data'].kids).length > 0 && layer < restrictMaxReplyLayer) {
                        //     let kidsComment = await getComments(commentData['data'].kids, (layer + 1));
                        //         replies[commentData['data'].id] = kidsComment;
                        // }

                        return commentData['data'];
                    }
                })
                .catch(error => {
                    console.log(error);
                });
            return currentComment;
        });
        const comments = await Promise.all(promise);
        return comments.filter(Boolean);
    };
    
	const minimize = (divId) => {
        // hiddenDivs[divId] = true;
        setHiddenDiv((prevState) => ({
            ...prevState,
            [divId]: true,
        }));
	};
    
	const maximize = (divId) => {
        setHiddenDiv((prevState) => ({
            ...prevState,
            [divId]: false,
        }));
        console.log(hiddenDivs)
	};

    return (
        <section className="content-body">
            <div className="main-item">
                <div className="title">
                    {itemData.title} 
                    <span className="url">
                        (
                        <a href={itemData.url}>
                            {itemData.displayUrl}
                        </a>
                        )
                    </span>
                </div>
                <div className="description">
                    {itemData.score} points by {itemData.by} {itemData.time} ago | {itemData.descendants} comments
                </div>
            </div>
            {
                comments.length !== 0 && 
                <div className="comment-section">
                    <h4 className="text-center">Comments</h4>
                    <div className="comment-wrapper">
                        {
                            comments.map(comment=>(
                                <div key={comment.id} className="comment-box main">
                                    <div className="name">
                                        {comment.by} <span>{comment.time} ago</span>
                                        <div className={hiddenDivs[comment.id] ? 'minimize hidden' : 'minimize'} title="Hide Comments" onClick={() => minimize(comment.id)}>[-]</div>
                                        <div className={!hiddenDivs[comment.id] ? 'maximize hidden' : 'maximize'} title="Show Comments" onClick={() => maximize(comment.id)}>[+]</div>
                                    </div>
                                    <div className="comment" dangerouslySetInnerHTML={{__html: comment.text}}>
                                    </div>
                                    <div id={comment.id} className={hiddenDivs[comment.id] ? 'hidden' : ''}>
                                        {
                                            comment.kidsComment.length > 0 && 
                                            comment.kidsComment.map(reply1=>(
                                                <div key={reply1.id} className="comment-box indent indent-1">
                                                    <div className="name">
                                                        {reply1.by} <span>{reply1.time} ago</span>
                                                        <div className={hiddenDivs[reply1.id] ? 'minimize hidden' : 'minimize'} title="Hide Comments" onClick={() => minimize(reply1.id)}>[-]</div>
                                                        <div className={!hiddenDivs[reply1.id] ? 'maximize hidden' : 'maximize'} title="Show Comments" onClick={() => maximize(reply1.id)}>[+]</div>
                                                    </div>
                                                    <div className="comment" dangerouslySetInnerHTML={{__html: reply1.text}}>
                                                    </div>
                                                    <div id={reply1.id} className={hiddenDivs[reply1.id] ? 'hidden' : ''}>
                                                        {
                                                            reply1.kidsComment.length > 0 && 
                                                            reply1.kidsComment.map(reply2=>(
                                                                <div id={reply2.id} key={reply2.id} className="comment-box indent indent-2">
                                                                    <div className="name">
                                                                        {reply2.by} <span>{reply2.time} ago</span>
                                                                        {/* <div className={hiddenDivs[reply2.id] ? 'minimize hidden' : 'minimize'} title="Hide Comments" onClick={() => minimize(reply2.id)}>[-]</div>
                                                                        <div className={!hiddenDivs[reply2.id] ? 'maximize hidden' : 'maximize'} title="Show Comments" onClick={() => maximize(reply2.id)}>[+]</div> */}
                                                                    </div>
                                                                    <div className="comment" dangerouslySetInnerHTML={{__html: reply2.text}}>
                                                                    </div>
                                                                </div>
                                                            ))
                                                        }
                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            }
        </section>
    );
}

export default Item
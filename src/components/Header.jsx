import React from 'react';

const Header = () => {
    return (
        <div className="text-center">
            <h1>Hacker News</h1>
            <span>Top News</span>
        </div>
    );
}

export default Header
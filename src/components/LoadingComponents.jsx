import React from 'react';

const LoadingComponent = () => {
    return (
        <div className='position-relative' 
            style={{
                zIndex: "9999",
                height: "100vh",
                width: "100vw",
                backgroundColor: "rgba(255, 255, 255, 0.45)",
                display: "flex"
            }}
        >
            <div className='text-center position-absolute'
                style={{
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    fontSize: "36px"
                }}
            >
                <span className='text-info '>
                    <i className="fas fa-circle-notch fa-spin"></i>
                </span>
                <p>
                    <em>
                        Loading ...
                    </em>
                </p>
            </div>
        </div>
    )
}

export default LoadingComponent;